//
//  LightControllerMock.h
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//

#ifndef __HomeAutomation__LightControllerMock__
#define __HomeAutomation__LightControllerMock__

#include "LightController.h"

#include "gmock/gmock.h"  // Brings in Google Mock.

class MockLightController : public I_LightController {
public:
    MOCK_METHOD1(On, void(unsigned id));
    MOCK_METHOD1(Off, void(unsigned id));
    MOCK_METHOD0(maxLights, unsigned(void));
};

#endif /* defined(__HomeAutomation__LightControllerMock__) */
