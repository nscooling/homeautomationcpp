//
//  LightScheduler.h
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//

#ifndef __HomeAutomation__LightScheduler__
#define __HomeAutomation__LightScheduler__

#include "TimeService.h"
#include "LightController.h"

class LightScheduler : public I_WakeUp
{
public:

    enum result_t { OK, TOO_MANY_EVENTS, ID_OUT_OF_BOUNDS, ID_NOT_FOUND };
    enum day_t {
        NONE=-1, EVERYDAY=10, WEEKDAY, WEEKEND,
        SUNDAY=1, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
    };
    LightScheduler::result_t ScheduleTurnOn(unsigned id, day_t day, unsigned minute);
    LightScheduler::result_t ScheduleTurnOff(unsigned id, day_t day, unsigned minute);
    LightScheduler::result_t ScheduleRemove(unsigned id, day_t day, unsigned minute);
    virtual void Wakeup(I_TimeService::Time time);
    
    LightScheduler(I_TimeService& ts, I_LightController& lc);
    ~LightScheduler();
    
private:
    
    enum event_t {
        TURN_OFF, TURN_ON,
    };
    
    static const int UNUSED = -1;
    
    struct ScheduledLightEvent
    {
        int id;
        event_t event;
        I_TimeService::Time time;
    };

    static const unsigned MAX_EVENTS = 128;
    ScheduledLightEvent scheduledEvents[MAX_EVENTS];
    
    I_TimeService& myTimeService;
    I_LightController& myLightController;
    
    LightScheduler::result_t scheduleEvent(unsigned id, day_t day, unsigned minute, event_t event);
    bool DoesLightRespondToday(I_TimeService::Time& time, int reactionDay);
    void operateLight(ScheduledLightEvent& lightEvent);
    void processEventDueNow(I_TimeService::Time& time, ScheduledLightEvent& lightEvent);
};



#endif /* defined(__HomeAutomation__LightScheduler__) */
