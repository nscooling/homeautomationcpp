#include "TimeServiceMock.h"
#include "LightControllerMock.h"

#include "LightScheduler.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"
using ::testing::AtLeast;
using ::testing::_;
using ::testing::Return;

class LightSchedulerTest : public ::testing::Test {
protected:
    virtual void SetUp() {
        EXPECT_CALL(ts, SetPeriodicAlarmInSeconds(60,_))
        .Times(AtLeast(1));
        
        ls = new LightScheduler(ts, lc);
    }
    
    virtual void TearDown() {
        EXPECT_CALL(ts, CancelPeriodicAlarmInSeconds(60,_))
        .Times(AtLeast(1));
        
        delete ls;
    }
    
    MockTimeService ts;
    MockLightController lc;
    LightScheduler* ls;
};



TEST_F(LightSchedulerTest, ScheduleTurnOn)
{
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(3, LightScheduler::WEEKEND, 1200));
}

TEST_F(LightSchedulerTest, IDNotFoundScheduleRemove)
{
    EXPECT_EQ(LightScheduler::ID_NOT_FOUND,ls->ScheduleRemove(3, LightScheduler::WEEKEND, 1200));
}

TEST_F(LightSchedulerTest, ScheduleThenRemove)
{
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(3, LightScheduler::WEEKEND, 1200));
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleRemove(3, LightScheduler::WEEKEND, 1200));
}

TEST_F(LightSchedulerTest, ScheduleWeekEndSaturdayIncluded)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::WEEKEND, mins));
    
    I_TimeService::Time time = { mins, LightScheduler::SATURDAY};
    
    EXPECT_CALL(lc, On(event_id))   // light should come on
    .Times(1);
    
    ls->Wakeup(time);
}

TEST_F(LightSchedulerTest, ScheduleWeekEndSundayIncluded)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::WEEKEND, mins));
    
    I_TimeService::Time time = { mins, LightScheduler::SUNDAY};
    
    EXPECT_CALL(lc, On(event_id))
    .Times(1);
    
    ls->Wakeup(time);
}

TEST_F(LightSchedulerTest, ScheduleWeekEndFridayExcluded)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::WEEKEND, mins));
    
    I_TimeService::Time time = { mins, LightScheduler::FRIDAY};

    EXPECT_CALL(lc, On(event_id))   // light should not come on
    .Times(0);
    
    ls->Wakeup(time);
}

TEST_F(LightSchedulerTest, ScheduleEverydayWithFriday)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::EVERYDAY, mins));
    
    I_TimeService::Time time = { mins, LightScheduler::FRIDAY};
    
    EXPECT_CALL(lc, On(event_id))   // light should come on
    .Times(1);
    
    ls->Wakeup(time);
}

TEST_F(LightSchedulerTest, ScheduleFridayWithFriday)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::FRIDAY, mins));
    
    I_TimeService::Time time = { mins, LightScheduler::FRIDAY};
    
    EXPECT_CALL(lc, On(event_id))   // light should come on
    .Times(1);
    
    ls->Wakeup(time);
}


TEST_F(LightSchedulerTest, ScheduleLightOnOff)
{
    int event_id = 3;
    int onMins = 1200;
    int offMins = 2400;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::EVERYDAY, onMins));
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOff(event_id, LightScheduler::EVERYDAY, offMins));
   
    I_TimeService::Time onTime  = {  onMins, LightScheduler::FRIDAY};
    I_TimeService::Time offTime = { offMins, LightScheduler::FRIDAY};
    
    
    EXPECT_CALL(lc, On(event_id))   // light should come on once
    .Times(1);
    EXPECT_CALL(lc, Off(event_id))   // light should go off once
    .Times(1);
    
    ls->Wakeup(onTime);
    ls->Wakeup(offTime);
}

TEST_F(LightSchedulerTest, ScheduleLightOnOffThenCancel)
{
    int event_id = 3;
    int onMins = 1200;
    int offMins = 2400;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::EVERYDAY, onMins));
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOff(event_id, LightScheduler::EVERYDAY, offMins));
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleRemove(event_id, LightScheduler::EVERYDAY, onMins));
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleRemove(event_id, LightScheduler::EVERYDAY, offMins));
    
    I_TimeService::Time onTime  = {  onMins, LightScheduler::FRIDAY};
    I_TimeService::Time offTime = { offMins, LightScheduler::FRIDAY};
    
    
    EXPECT_CALL(lc, On(event_id))   // light should not come on
    .Times(0);
    EXPECT_CALL(lc, Off(event_id))   // light should not go off
    .Times(0);
    
    ls->Wakeup(onTime);
    ls->Wakeup(offTime);
}

TEST_F(LightSchedulerTest, exhaustEventQueue)
{
    int event_id = 3;
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    for(int i = 0; i < 128; ++i) {
        EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(event_id, LightScheduler::FRIDAY, mins));
    }
    
    // now expect failure
    EXPECT_EQ(LightScheduler::TOO_MANY_EVENTS,ls->ScheduleTurnOn(event_id, LightScheduler::FRIDAY, mins));
}

TEST_F(LightSchedulerTest, testIDBounds)
{
    int mins = 1200;
    
    EXPECT_CALL(lc, maxLights())
    .WillRepeatedly(Return(32));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(0, LightScheduler::FRIDAY, mins));
    
    EXPECT_EQ(LightScheduler::OK,ls->ScheduleTurnOn(31, LightScheduler::FRIDAY, mins));
    
    //  expect failure
    EXPECT_EQ(LightScheduler::ID_OUT_OF_BOUNDS,ls->ScheduleTurnOn(32, LightScheduler::FRIDAY, mins));
}
